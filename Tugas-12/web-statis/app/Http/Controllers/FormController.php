<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form(){
        return view('form');
    }

    public function kirim(Request $request){
        $firstName =  $request['fname'];
        $lastName = $request['lname'];
        return view('welcome', ['FirstName' => $firstName, 'LastName' => $lastName]);
    }
}
