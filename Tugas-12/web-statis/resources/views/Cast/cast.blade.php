@extends('master')
@section('judul')
Cast
@endsection
@section('subtitle')
Halaman Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Data</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key+1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>
                            <a href="/cast/{{ $value->id }}" class="btn btn-info">Detail</a>
                        </td>
                        <td style="display: flex;">
                            <a style="margin: 5px;" href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form style="margin: 5px;" action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                                </form>
                        </td>
                        </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection