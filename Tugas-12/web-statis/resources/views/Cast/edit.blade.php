@extends('master')
@section('judul')
Cast Edit
@endsection
@section('subtitle')
Halaman Cast Edit
@endsection

@section('content')
<div>
    <h2>Edit Data</h2>
        <form action="/cast/{{ $cast->id }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" value= "{{ $cast->nama }}" name="nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" name="umur" value="{{ $cast->umur }}">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control" name="bio" cols="30" rows="10">{{ $cast->bio }}
                </textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
@endsection