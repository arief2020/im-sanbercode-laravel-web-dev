<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="firstname"> First name:</label><br><br>
        <input type="text" name="fname"><br><br>
        <label for="lastname">Last name:</label><br><br>
        <input type="text" name="lname"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender" id="male" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" id="other" value="other">
        <label for="other">Other</label><br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="filiphine">Filiphine</option>
            <option value="japanese">Japanese</option>
        </select>
        <br><br>
        <label for="langange">Languange Spoken:</label><br><br>
        <input type="checkbox" name="bahasa_indonesia" id="bahasa_indonesia">
        <label for="bahasa_indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="english" id="english">
        <label for="english">English</label><br>
        <input type="checkbox" name="other" id="other">
        <label for="other">Other</label><br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>