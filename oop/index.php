<?php
require_once 'animal.php';
require_once 'Ape.php';
require_once 'Frog.php';

$sheep = new Animal("shaun");


echo "Name : ";
echo $sheep->name; // "shaun"
echo "<br>";
echo "legs : ";
echo $sheep->legs; // 4
echo "<br>";
echo "cold blooded : ";
echo $sheep->cold_blooded; // "no"
echo "<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Name : ";
echo $kodok->name; // "shaun"
echo "<br>";
echo "legs : ";
echo $kodok->legs; // 4
echo "<br>";
echo "cold blooded : ";
echo $kodok->cold_blooded; // "no"
echo "<br>";
echo "Jump : ";
$kodok->jump() ; // "hop hop"
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : ";
echo $sungokong->name; // "shaun"
echo "<br>";
echo "legs : ";
echo $sungokong->legs; // 4
echo "<br>";
echo "cold blooded : ";
echo $sungokong->cold_blooded; // "no"
echo "<br>";
echo "Yell : ";
$sungokong->yell(); // "Auooo"
?>